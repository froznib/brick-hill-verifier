FROM node:12-slim
COPY . ./verifier
WORKDIR /verifier
RUN npm install
CMD npm run start